from asyncio import Semaphore, gather
from datetime import datetime
from aiohttp import ClientSession
import requests
from sqlalchemy.ext.asyncio import AsyncSession
from views.ads_sections import AdsCampaign
from sqlalchemy import select
from views.sellers import Seller
import asyncio
from loguru import logger
from database import AsyncSessionLocal


async def async_get_campign(session: ClientSession,
                            advert_id: int,
                            token: str,
                            semaphore: Semaphore):
    async with semaphore:
        result = await session.get(
            url="https://advert-api.wb.ru/adv/v0/advert",
            headers={"Authorization": token},
            params={"id": advert_id})
        if result.status == 200:
            return AdsCampaign(**await result.json())



async def get_campaings(seller: Seller,
                        status: int = None,
                        type: int = None,
                        limit: int = None,
                        offset: int = None,
                        order: str = None,
                        direction: str = None):

    response = requests.get(
        url="https://advert-api.wb.ru/adv/v0/adverts",
        headers={"Authorization": seller.ads_wb_token},
        params={
            "status": status or "",
            "type": type or "",
            "limit": limit or "",
            "offset": offset or "",
            "order": order or "",
            "direction": direction or ""
        }
    )

    if response.status_code == 204:
        logger.warning(f"У seller = {seller.company_name} нет активных компаний")
        return
    
    if response.status_code != 200:
        logger.warning(f"status code = {response.status_code} seller = {seller.company_name}")
        return

    if response.json() is None:
        return []
    
    was = len(response.json())
    all_adverts = [AdsCampaign(**i) for i in response.json()]
    
    semaphore = Semaphore(10)
    async with ClientSession() as session:
        tasks = [
            async_get_campign(
                session,
                i.advertId,
                seller.ads_wb_token,
                semaphore
            ) for i in all_adverts
        ]
        responses = await gather(*tasks)
    became = len(responses)
    logger.info(f"Всего компаний у {seller.company_name} =  {was} Активных компаний = {became}")
    return responses



async def start():
    async with AsyncSessionLocal() as session:

        sellers_cursor_result = await session.execute(select(Seller))
        sellers = sellers_cursor_result.scalars().all()

        active_campaning: dict[str, AdsCampaign] = {}
        for seller in sellers:
            active_campaning[seller.company_name] = await get_campaings(seller, status=9)

 
if __name__ == "__main__":

    logger.add(
        "logs/{}".format(datetime.now().strftime("%d_%m_%Y.log")),
        format="{time} {level} {message}",
        level="DEBUG")
    loop = asyncio.get_event_loop()
    loop.run_until_complete(start())
