from datetime import datetime
from enum import Enum
from pydantic import BaseModel
from .utils import to_camel


class AdsSectionBaseScheme(BaseModel):

    class Config:
        orm_mode = True
        alias_generator = to_camel
        allow_population_by_field_name = True


class AdsCampaign(AdsSectionBaseScheme):

    advertId: int
    type: int
    status: int
    dailyBudget: int
    name: str | None = None
    createTime: datetime
    changeTime: datetime
    startTime: datetime
    endTime: datetime
    searchPluseState: bool | None = None
