from datetime import datetime
from database import Base
from sqlalchemy import Column, ForeignKey, Integer, String, TIMESTAMP
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship
from views.users import User


class Seller(Base):

    __tablename__ = "sellers"

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("users.id"))

    wb_token = Column(String, nullable=True)
    standart_wb_token = Column(String, nullable=True)
    statistics_wb_token = Column(String, nullable=True)
    passport_token = Column(String, nullable=True)
    ads_wb_token = Column(String, nullable=True)
    company_name = Column(String, nullable=True)
    wb_article = Column(Integer, nullable=True)

    created_at = Column(TIMESTAMP, default=datetime.utcnow(), nullable=False)
    updated_at = Column(TIMESTAMP, default=datetime.utcnow(),
                        nullable=False, onupdate=func.now())
    deleted_at = Column(TIMESTAMP, default=None, nullable=True)

    user = relationship(
        argument=User,
        backref="users")
